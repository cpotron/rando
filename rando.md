# Synchro pré-rando

## Transports

### Aller

Horaires du bus 965 Lourdes -> Cauterets le lundi 9
  - 08:23 > 09:26
  - 12:50 > 13:53
  - 16:55 > 17:58
  - 18:05 > 19:08

### Retour

Horaires du bus 965 Cauterets -> Lourdes le dimanche 15
  - 10:45 > 11:48
  - 15:05 > 16:08
  - 18:10 > 19:13

## Check-list

- Répartition des tentes :
  - Corentin (x2 places)
  - Nicolas (x2 places)
  - Théo (x3 places)
  - Thibault (x3 places)
- Sac à dos :
  - Thibault ramène un sac 60L pour Achille
  - Penser à prendre des sacs congélo pour housse étanche / compacter les affaires
- Pour faire la bouffe :
  - Brûleurs Easy Clic Plus Valve (Corentin x1, Valentine x1)
  - Pare-vent (Corentin x1)
  - Briquets (Corentin x3)
  - Recharges Campingaz CV 470 Plus (Corentin x3, Valentine x1)
  - Bouilloire (Corentin x1)
  - Casserole (Thibault x1)
  - Couverts (Chacun les siens)
  - Gamelle
    - Thibault en ramène une en plus pour Achille
  - Tasses / Mug
    - Thibault en ramène deux pour Achille et Théo
- Vêtements
  - Chaussures de randonnée
  - Polaire pour les nuits
  - Tee-shirts
  - Shorts / pantacourts / pantalons  
  - Tenue ville "propre"
  - Sous-vêtements
  - Grosses chaussettes de randonnée / de ski
- Se protéger du soleil :
  - Chapeau / casquette
  - Lunettes de soleil
  - Tour de cou / bandana
  - Crème solaire :
    - Corentin x2
    - Valentine x3/4
    - Adrien x3/4
- Pour la nuit :
  - Masque anti-lumière
  - Bouchons anti-bruit
  - Sac de couchage
  - Matelas gonflable / tapis de sol
  - Lampe frontale (chacun la sienne)
    - Sauf Thibault qui ramène à la place une lampe tempête
    - Chacun son jeu de piles individuelles pour les lampes
- Pour l'eau :
  - Serviette
  - Maillot de bain
  - Si pluie :
    - Poncho imperméable
    - Protection pour sac à dos intégrée
- Trousse de toilette / hygiène :
  - Savon
    - Théo x3
    - Arnaud x2
  - PQ
    - Arnaud x2
    - Achille x4 (à acheter)
  - Brosse à dents (chacun la sienne)
  - Dentifrice
    - Thibault x1
    - Corentin x1
  - Brosse
  - Répulsifs :
    - anti-moustique
    - anti-tiques (pas nécessaire du tout)
  - Beaume à lèvres (chacun le sien)
  - Mini-déodorant (chacun le sien ?)
  - Sac poubelle individuel
- Trousse de secours :
  - Les ampoules aux pieds (chacun les siens) :
    - Bandes anti-ampoules (exemple : https://www.mercurochrome.fr/product/sparadrap-ampoules/)
    - Pansements pour soigner les ampoules (exemple : https://www.compeed.fr/compeed-interphase-page/)
    - Pansements à découper / divers / bandages
  - Antiseptique
    - Corentin x1
    - Théo x1
  - Pince à épiler
    - Théo x1
  - Doliprane
    - Théo x1
    - Valentine x1
  - Smecta
    - Corentin x1
  - Tire-tique
    - Corentin x1
  - Gel hydro alcoolique (chacun le sien)
  - Couverture de survie
    - Théo x1
  - Apaisyl
    - Corentin x1
- Papiers :
  - Carnet de vaccination
  - Carte de mutuelle / vitale
  - Argent liquide / CB
  - Pass sanitaire EN PAPIER (en + de la version PDF sur téléphone)
  - Carte topo (géré par Corentin)
  - Pelle caca :
    - Thibault x1
- Pour boire (eau individuelle) :
  - Bidons d'eau
  - Moyen de purifier l'eau
    - Thibault ramène des pastilles Micropur
  - Alcool :
    - Sobre la semaine, samedi soir je promet rien
- Pour manger (bouffe individuelle) :
  - Barres énergétiques (comptez au moins 2 / jours donc 10 environ)
  - Lyophilisés (déjà gérés dans le gdoc correspondant)
  - Saucisson
  - Bonbons (Achille :heart:)
- Jeux :
  - Ramenez ce que vous voulez !
